
    btnCalcular.addEventListener("click", function () {
        let alt = document.getElementById('txtAltura').value;
        let ps = document.getElementById('txtPeso').value;
        let genH = document.getElementById("generoHombre");

        // Calculo del Indice de Masa Corporal

        let imc = ps / (alt * alt);
        document.getElementById('txtIMC').value = imc.toFixed(2);
    
        // Selección y visualización de la imagen en base al IMC

        let imagen = document.getElementById("img");
        document.getElementById("img").style.display = "inline";

        if (imc >= 40) {
            if (genH.checked) {
                imagen.src = "/IMG/06H.png";
            } else {
                imagen.src = "/IMG/06M.png";
            }
        } else if (imc >= 35 && imc < 40) {
            if (genH.checked) {
                imagen.src = "/IMG/05H.png";
            } else {
                imagen.src = "/IMG/05M.png";
            }
        } else if (imc >= 30 && imc < 35) {
            if (genH.checked) {
                imagen.src = "/IMG/04H.png";
            } else {
                imagen.src = "/IMG/04M.png";
            }
        } else if (imc >= 25 && imc < 30) {
            if (genH.checked) {
                imagen.src = "/IMG/03H.png";
            } else {
                imagen.src = "/IMG/03M.png";
            }
        } else if (imc >= 18.5 && imc < 25) {
            if (genH.checked) {
                imagen.src = "/IMG/02H.png";
            } else {
                imagen.src = "/IMG/02M.png";
            }
        } else if (imc < 18.5) {
            if (genH.checked) {
                imagen.src = "/IMG/01H.png";
            } else {
                imagen.src = "/IMG/01M.png";
            }
        }

        // Selección de la cantidad de calorias en base a la tabla

        let age = document.getElementById("txtEdad").value;
        let cal = 0;

        if (age >= 10 && age < 18){
            if (genH.checked){
                cal = (17.686 * ps) + 658.2;
            } else{
                cal = (13.384 * ps) + 692.6;   
            }
        }else if (age >= 18 && age < 30){
            if (genH.checked){
                cal = (15.057 * ps) + 692.2;
            } else{
                cal = (14.818 * ps) + 486.6;   
            }
        }else if (age >= 30 && age < 60){
            if (genH.checked){
                cal = (11.472 * ps) + 873.1;
            } else{
                cal = (8.126 * ps) + 845.6;   
            }
        }else if (age >= 60){
            if (genH.checked){
                cal = (11.711 * ps) + 587.7;
            } else{
                cal = (9.082 * ps) + 658.5;   
            }
        }
        

        document.getElementById("txtCal").value = cal.toFixed(2);
        document.getElementById("showIMC").style.display = "inline"; document.getElementById("txtIMC").style.display = "inline";
        document.getElementById("showCal").style.display = "inline"; document.getElementById("txtCal").style.display = "inline";
    });
    