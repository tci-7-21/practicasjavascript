document.getElementById("btnCalcular").addEventListener("click", function() {
    // Variables

    let alt = document.getElementById('txtAltura').value;
    let ps = document.getElementById('txtPeso').value;

    // Al hacer clic en el botón, muestra el label y el input del IMC
    document.getElementById("IMC").style.display = "inline";
    document.getElementById("showIMC").style.display = "inline";

    // Calculo

    let tot = ps / (alt * alt);
    document.getElementById("showIMC").innerHTML = tot.toFixed(2);
});