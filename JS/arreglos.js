const boton = document.getElementById('Crear');
//Funcion que regresa el promedio de los elementos del arreglo

boton.addEventListener('click', function(){
    //Creacion del arreglo
    const cmbAleatorios = document.getElementById('cmbNumeros');
    let x = document.getElementById('numero').value;
    arr = [];
    cmbAleatorios.innerHTML = " ";
    for(i=0;i<x;i++){
        arr[i]=Math.floor(Math.random()*1000);

        let option = document.createElement('option');
        option.value=arr[i];

        option.innerHTML=arr[i];
        cmbAleatorios.appendChild(option);
        
    }


    //Promedio
    let promedio = 0;

    for(i=0;i < arr.length; i++){
        promedio += arr[i];
    }

    promedio = promedio/arr.length;

    document.getElementById('promedio').value = ("Promedio: "+ promedio.toFixed(2));

    //Mayor valor
    let mayor = 0;
    let posM = 0;

    for(i=0; i<arr.length;i++){
        if(arr[i]>=mayor){
            mayor=arr[i];
            posM = i+1;
        }
    }

    document.getElementById('mayor').value=("Mayor: " + mayor + ", Indice: " + posM);


    //Menor valor

    let menor = 9999;
    let posm = 0;

    for(i=0; i<arr.length;i++){
        if(arr[i]<=menor){
            menor=arr[i];
            posm = i+1;
        }
    }

    document.getElementById('menor').value=("Menor: " + menor + ", Indice: " + posm);

    //Porcentaje de Pares y Impares

    let pares = 0;
    let impares = 0;

    for(i=0;i<arr.length;i++){
        if(arr[i]%2==0){
            pares++;
        }else{
            impares++;
        }
    }

    document.getElementById('pares').value=("Porcentaje de Pares: " + (pares/arr.length*100).toFixed(0) + "%")
    document.getElementById('impares').value=("Porcentaje de Impares: " + (impares/arr.length*100).toFixed(0) + "%")

    //Simetrico

    if((pares/arr.length*100) >= (impares/arr.length*100) +20){
        document.getElementById('simetrico').value=("Simetrico: Si");
    }else{
        document.getElementById('simetrico').value=("Simetrico: No");
    }

});